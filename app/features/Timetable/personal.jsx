import { ipcRenderer } from 'electron'
import React, { Component } from 'react'
import { PersonalReport } from './personalReport'
import { Table, Button, Row, Col, Select } from 'antd'
import { PersonalInvigilation, getSelector } from '../_shared/services/dataService'
import '../Teachers/teachers'

const columns: Array<Object> = [
  { title: 'SN', dataIndex: 'pid', key: 'id' },
  // { title: 'Title', dataIndex: 'title', key: 'title' },
  { title: 'Date', dataIndex: 'date', key: 'date' },
  { title: 'Time/mins', dataIndex: 'duration_total', key: 'duration_total' },
  { title: 'Status', dataIndex: 'status', key: 'status' },
  { title: 'Rate/Hr', dataIndex: 'rate_hr', key: 'rate_hr' },
  { title: 'Amt(GHC)', dataIndex: 'amount', key: 'amount' },
  { title: 'Tax(GHC)', dataIndex: 'tax', key: 'tax' },
  { title: 'Amt Due(GHC)', dataIndex: 'amount_due', key: 'amount_due' },
  { title: 'Meal (GHC)', dataIndex: 'snack_allowance', key: 'snack_allowance' },
  { title: 'Day-Total(GHC)', dataIndex: 'day_total', key: 'day_total' }
]

const Option = Select.Option
const GENERATION_SUCCESS_CHANNEL_NAME = 'GENERATION_SUCCESS'
const GENERATION_CHANNEL_NAME = 'GENERATION'
var app = require('electron').remote
var dialog = app.dialog
var fs = require('fs')

function arrayToCSV(objArray) {
  const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
  let str = `${Object.keys(array[0]).map(value => `"${value}"`).join(",")}` + '\r\n';

  return array.reduce((str, next) => {
      str += `${Object.values(next).map(value => `"${value}"`).join(",")}` + '\r\n';
      return str;
     }, str);
}
class Personal extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      dataSource: [],
      name: '',
      sem: '',
      year: '',
      width: 0,
      height: 0,
      type: 'Mid Semester',
      staff: ""
    }

    this.updateWindowDimensions = this.updateWindowDimensions.bind(this)
  }


  handleBlur () {
    console.log('blur')
  }

  handleFocus () {
    console.log('focus')
  }

  updateWindowDimensions () {
    this.setState({ width: window.innerWidth, height: window.innerHeight })
  }

  componentDidMount () {
    this.updateWindowDimensions()
    window.addEventListener('resize', this.updateWindowDimensions)
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.updateWindowDimensions)
  }

  renderStaffData = () => {
    const staff = getSelector('personnel').map((element, index) => {
      // console.log(element.name);
      return (
        <Option value={element.name} key={element.index}>
          {element.name}
        </Option>
      )
    })

    return staff
  }

  handleChange = (value) => {
    this.setState({
      type: value
    })
  }

  handleStaffChange = (value) => {
    this.setState({
      staff: value
    })
  }

  render () {
    const dataSource = PersonalInvigilation(this.state.staff).map((element, id) => {
      return {
        ...element,
        pid: id + 1,
        key: id
      }
    }).filter(element => element.type === this.state.type)

    const handleGeneration = () => {
      let termDetails = {
        year: this.state.year,
        sem: this.state.sem
      }

      var content = arrayToCSV(dataSource);

      dialog.showSaveDialog(fileName => {
        if (fileName === undefined) {
          swal ( "Oops" ,  "Something went wrong! we couldn't create your file" ,  "error" )
          return
        }

        // fileName is a string that contains the path and filename created in the save file dialog.
        fs.writeFile(fileName, content, err => {
          if (err) {
            swal ( "Oops" ,  "Something went wrong! we couldn't write your file" ,  "error" )
          }

          swal ( "Good job!" ,  "File created successfully!" ,  "success" )
        })
      })
      // ipcRenderer.send(GENERATION_CHANNEL_NAME, PersonalReport(dataSource, termDetails))
    }

    return (
      <div>
        <div className='search-bar-person'>
          <Row style={{ marginBottom: '1rem' }}>
            <Col span={8} style={{ float: 'left' }}>
              <div>
                {/* <p style={{ margin: '0rem 5rem' }} className='report-termdetails'>
              <Icon type='edit' style={{ paddingRight: '0.5rem' }} />
              Exam type
            </p> */}
                <Select
                  defaultValue='Mid Semester'
                  style={{ margin: '0rem 1rem', width: '80%' }}
                  onChange={e => this.handleChange(e)}
                  size='large'
                >
                  <Option value='Mid Semester'>Mid Semester</Option>
                  <Option value='End of Semester'>End of Semester</Option>
                </Select>
              </div>
            </Col>
            <Col span={8} style={{ float: 'left' }}>
              <div>
                {/* <p style={{ margin: '0rem 5rem' }} className='report-termdetails'>
              <Icon type='edit' style={{ paddingRight: '0.5rem' }} />
              Exam type
            </p> */}
                <Select
                  showSearch
                  style={{ width: '100%' }}
                  placeholder='Select a person'
                  optionFilterProp='children'
                  size='large'
                  onChange={e => this.handleStaffChange(e)}
                  onFocus={this.handleFocus()}
                  onBlur={this.handleBlur()}
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {this.renderStaffData()}
                </Select>
              </div>
            </Col>
            <Col span={8}>
              <div>
                <Button
                  type='primary'
                  size='large'
                  className='generate-button'
                  onClick={() => handleGeneration()}
                  style={{ float: 'right' }}
                  // disabled={!this.state.year && !this.state.year}
                >
                  Generate Report
                </Button>
              </div>
            </Col>
          </Row>
        </div>
        <div className='table-container'>
        <div className='table-generater-container'>
          <Table
            className='snack-list-table'
            pagination={{ pageSize: this.state.height / 70 }}
            dataSource={dataSource}
            style={{ margin: '0rem 1rem' }}
            columns={columns}
          />
        </div>
      </div>
      </div>
    )
  }
}

export default Personal
