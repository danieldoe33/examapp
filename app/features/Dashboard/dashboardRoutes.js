import Timetable from '../Timetable';
import Classes from '../Classes';
import Courses from '../Courses';
import ErrorComponent from '../Error';
import Teachers from '../Teachers';
import Settings from '../Settings';

export default [
    {
        title: 'Allocations',
        exact: true,
        path: '/',
        component: Timetable.ProgrammeTT
    },
    {
        title: 'Report',
        exact: true,
        path: '/timetable/teachers',
        component: Timetable.TeacherTT
    },
    {
        title: 'Staff members',
        exact: true,
        path: '/settings/teachers',
        component: Teachers
    },
    {
        title: 'Exam configurations',
        exact: true,
        path: '/settings/college',
        component: Settings.CollegeSettings
    },
    {
        title: 'Data reset',
        exact: true,
        path: '/settings/reset',
        component: Settings.DataReset
    }
    // {
    //     title: 'Error Page',
    //     exact: true,
    //     path: '/settings/rrror',
    //     component: ErrorComponent
    // }
];
