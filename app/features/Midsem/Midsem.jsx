import React from 'react';
import { connect } from 'react-redux';
import { Modal, Icon } from 'antd';

import './midsem.css';
import { addPackage, updatePackage, deletePackage } from '../_shared/services/dataService';
import { NewMidsem } from './NewMidsem';
import MidsemList from './midList';
import * as Actions from './midActions';
import swal from 'sweetalert';


const DEFAULT_EDIT_CONFIG = {
    // Edit configuration
    editMode: false,   // Flag to indicate whether we are editting or adding
    editID: -1
};

const confirm = Modal.confirm;

class Midsem extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            ...DEFAULT_EDIT_CONFIG, 
            field: {},
            selected_class: [],
            visible: false
        };

        this.midsemEditted = this.midsemEditted.bind(this);
        this.cancelEditMode = this.cancelEditMode.bind(this);
        this.midsemRemoved = this.midsemRemoved.bind(this);
        this.triggerEditMode = this.triggerEditMode.bind(this);
    }

    componentWillMount() {
        document.title = 'Mid Semester Configuration';
 
        const { midsem } = this.props;

        // check redux for data else use database data

        if (midsem.length === 0) {
            this.props.midsemLoaded();
        }
    }

    // Return from the Edit mode to the Add mode
    cancelEditMode() {
        this.setState({
            ...DEFAULT_EDIT_CONFIG,
            editMode: false
        });
    }

    midsemEditted(midsemDetail: Object) {
        if (this.state.editMode === true && midsemDetail.id !== -1) {
            // We are in edit mode. Find the teacher and splice the list
            // const doEditing = (array: Array<Object>) => {
            //     return array.map(element => (element.id === teacher.id) ? teacher : element);
            // };
            console.log(midsemDetail, this.state.editMode);

            this.props.midsemEditted(midsemDetail);
            updatePackage(midsemDetail);
            swal("Good job!", "Record was updated succesfully", "success");
            // updateVenue(midsemDetail);
            this.cancelEditMode();

        } else {
            this.props.midsemAdded(midsemDetail);
            addPackage(midsemDetail);
            swal("Good job!", "Record was added succesfully", "success");
            // addVenue(midsemDetail); 
        }

        this.cancelEditMode();
    }

    handleOk = () => {
        this.setState({
            visible: false,
          });
    }

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    }

    midsemRemoved(midsemDetail: Object) {
        return (
            confirm({
                title: `Do you want to delete package with ${midsemDetail.session_count} session?`,
                content: `When the Yes button is clicked, ${midsemDetail.session_count} session will be 
                deleted permanently with all data related to it.
                Please proceed with caution`,
                okText: 'Yes',
                visible: this.state.visible,
                okType: 'danger',
                cancelText: 'No',
                onOk: () => { this.props.midsemRemoved(midsemDetail); this.handleOk(); deletePackage(midsemDetail); },
                onCancel: () => this.handleCancel()
              })
        );
    }

    triggerEditMode(midsemDetail: Object) {
        // eslint-disable-next-line
        this.setState({
            // the -1 is because we added +1 when rendering the table
            editID: midsemDetail.id,
            editMode: true,
            field: midsemDetail
        });
    }

    render() {
        return (
            <div id="midsem" className="grid">
                <MidsemList
                    onEditClicked={this.triggerEditMode}
                    onDeleteClicked={this.midsemRemoved}
                    dataSource={this.props.midsem} />

                <NewMidsem
                    editMode={this.state.editMode}
                    id={this.state.editID}
                    fieldData={this.state.field}
                    midsem={this.props.midsem}
                    onCancel={this.cancelEditMode}
                    onMidsemEditted={this.midsemEditted} />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        midsem: state.midsem
    };
};

export default connect(mapStateToProps, { ...Actions })(Midsem);
