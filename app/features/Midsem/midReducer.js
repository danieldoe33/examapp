import initialStoreState from '../../store/initialStoreState';
import {ActionTypes} from '../../constants';

const initialState = initialStoreState.midsem;
const types = ActionTypes.midsem;

export default function (state = initialState, action) {
    let newstate = state;
    switch (action.type) {
        case types.MIDSEMS_ADDED:
            return action.payload;

        case types.MIDSEM_ADDED:
            newstate = state;
            newstate.push({
                ...action.payload,
                id: state.length
            });
            return newstate;

        case types.MIDSEM_REMOVED:
            newstate = state;
            console.log("new state: ", newstate, "action", action.payload);
            newstate = state.filter((elem: Object) => elem.session_count !== action.payload.session_count);
            return newstate;

        case types.MIDSEM_EDITTED:
            newstate = state;
            console.log("new state: ", newstate, "action", action.payload);
            newstate = newstate.map(element => ((element.session_count === action.payload.session_count) ? action.payload : element));
            return newstate;
        
        default:
            return state;     
    }        
}
