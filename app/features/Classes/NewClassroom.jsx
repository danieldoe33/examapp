import React from 'react'
import PropTypes from 'prop-types'
import { Form, Icon, Input, Button, Select, InputNumber, Row, Col } from 'antd'
import { getSelector } from '../_shared/services/dataService'
import swal from 'sweetalert'

const FormItem = Form.Item
const Option = Select.Option

class NewClassroomForm extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      counter: 1
    }

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
    // To disabled submit button at the beginning and checks validations.
    this.props.form.validateFields()
  }

  componentWillReceiveProps (nextProps) {
    if (Object.keys(nextProps.fieldData).length !== 0 && nextProps.editMode === true && this.state.counter > 0) {
      this.props.form.setFieldsValue({
        session: nextProps.fieldData.session_count,
        snack_count: nextProps.fieldData.snack_count,
        amount: nextProps.fieldData.amount
      })
      this.setState({ counter: -1 })
    }
  }

  // reset form data when submitted
  handleReset = () => {
    this.props.form.resetFields()
    this.setState({ counter: 1 })
  }

  indexError = () => {
    swal("Oops!", "We encontered an error trying to add your item", "error");
  }

  // function called when the button is selected
  handleSubmit = e => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      const id = this.props.editMode ? this.props.id : -1

      let hasError: boolean = false
      this.props.classes.forEach((classroom: Object) => {
        if (classroom.session_count === values.session && this.props.editMode === false && classroom.type === "End of Semester") {
          hasError = true
        }
      })

      if (hasError) {
        this.indexError()
        return
      }

    //   const cashItem = getSelector('cash_item')
    //   console.log(cashItem)
      // const snackPrice = cashItem.filter(item => item.item === "Snack")[0].item_amount;
      // const lunchPrice = cashItem.filter(item => item.item === "Lunch")[0].item_amount;

      if (!err) {
        const classDetail: Object = {
          id: this.props.id,
          session_count: values.session,
          snack_count: values.snack_count,
          amount: values.amount,
          type: 'End of Semester'
        }
        // console.log(classDetail);
        this.props.onClassEditted(classDetail)
      }

      // const {getFieldDecorator, getFieldsError, getFieldError, isFieldTouched} = this.props.form;
      this.handleReset()
    })
  }

  // this checks the form validation
  hasErrors (fieldsError) {
    const fromAnt = Object.keys(fieldsError).some(field => fieldsError[field])
    return fromAnt
  }

  renderCancel () {
    return this.props.editMode === false ? null : (
      <button
        type='button'
        style={{ margin: '0px auto', width: '100%' }}
        onClick={() => {
          this.props.onCancel()
          this.handleReset()
        }}
      >
        Cancel
      </button>
    )
  }

  render () {
    const header: string = this.props.editMode ? 'Edit' : 'New'
    const buttonText: string = this.props.editMode ? 'Edit' : 'Add'

    const { getFieldDecorator, getFieldsError, getFieldError, isFieldTouched, getFieldValue } = this.props.form
    const classNameError = isFieldTouched('session') && getFieldError('session')
    const classSizeError = isFieldTouched('snack_count') && getFieldError('snack_count')
    const examsError = isFieldTouched('amount') && getFieldError('amount')
    // const othersError = getFieldError('otherSize');

    const className = getFieldValue('session')
    const classSize = getFieldValue('snack_count')
    const examSize = getFieldValue('amount')

    const isEmpty = !className || !classSize || !examSize;

    return (
      <div>
        <Form onSubmit={this.handleSubmit} className='column new-classroom'>
          <h2>{header} configuration </h2>
          <label htmlFor='new-classroom-name'>Session</label>
          <FormItem
            style={{ textAlign: '-webkit-center' }}
            hasFeedback
            // label="Username"
            validateStatus={classNameError ? 'error' : ''}
            help={classNameError || ''}
          >
            {getFieldDecorator('session', {
              rules: [{ required: true, type: 'number', message: 'enter session!' }]
            })(<InputNumber min={1} max={10} style={{ width: '100%' }} placeholder='e.g. ' />)}
          </FormItem>
          <label htmlFor='new-classroom-std-cap'>Snack</label>
          <FormItem
            // style={{textAlign: '-webkit-center'}}
            hasFeedback
            validateStatus={classSizeError ? 'error' : ''}
            help={classSizeError || ''}
          >
            {getFieldDecorator('snack_count', {
              rules: [
                {
                  required: true,
                  type: 'number',
                  message: 'snack!'
                }
              ]
            })(<InputNumber min={1} max={10} style={{ width: '100%', marginRight: '0.5rem' }} placeholder='e.g. 50' />)}
          </FormItem>
          <label htmlFor='new-classroom-std-cap'>Amount</label>
          <FormItem
            // style={{textAlign: '-webkit-center'}}
            hasFeedback
            validateStatus={examsError ? 'error' : ''}
            help={examsError || ''}
          >
            {getFieldDecorator('amount', {
              rules: [
                {
                  required: true,
                  type: 'number',
                  message: 'session amount!'
                }
              ]
            })(<InputNumber min={1} max={1000} style={{ width: '100%', marginRight: '0.5rem' }} placeholder='e.g. 50' />)}
          </FormItem>
          <FormItem>
            <Button
              type='primary'
              size={'large'}
              // className=""
              style={{ margin: '20px auto', width: '100%', backgroundColor: '' }}
              htmlType='submit'
              disabled={this.hasErrors(getFieldsError()) && isEmpty}
            >
              {buttonText} configuration
            </Button>
          </FormItem>
          {this.renderCancel()}
        </Form>
      </div>
    )
  }
}

export const NewClassroom = Form.create()(NewClassroomForm)

NewClassroom.defaultProps = {
  id: -1,
  name: '',
  classSize: '',
  otherSize: ''
}

NewClassroom.propTypes = {
  // Edit configuration
  editMode: PropTypes.bool.isRequired,
  id: PropTypes.number,
  name: PropTypes.string,
  classSize: PropTypes.string,
  otherSize: PropTypes.string,

  onClassEditted: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
}
