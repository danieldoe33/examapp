import React from 'react';
import PropTypes from 'prop-types';
import Table from 'antd/lib/table';
import { Input, Row, Col } from 'antd';
import { getSelector } from '../_shared/services/dataService';

const Search = Input.Search;

export default class TeacherList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			dataSource: [],
			dataSearch: this.props.dataSource,
			width: 0,
			height: 0,
		};

		this.state.dataSource = this.props.dataSource;
		this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			dataSource: nextProps.dataSource,
		});
	}

	updateWindowDimensions() {
		this.setState({ width: window.innerWidth, height: window.innerHeight });
	}

	componentDidMount() {
		this.updateWindowDimensions();
		window.addEventListener('resize', this.updateWindowDimensions);
	}

	componentWillUnmount() {
		window.removeEventListener('resize', this.updateWindowDimensions);
	}

	onSearch = e => {
		// console.log(e.target.value)
		const value = e.target.value.toLowerCase();
		const newData = this.state.dataSource.filter(s => s.name.toLowerCase().search(value) !== -1);
		// console.log("newData: ", newData);
		this.setState({ dataSearch: newData });
	};

	render() {
		const dataSource = getSelector('personnel').map((element, id) => {
			return {
				...element,
				pid: id + 1,
				key: id,
			};
		});

		const columns: Array<Object> = [
			{ title: 'ID', dataIndex: 'pid', key: 'pid' },
			// { title: 'Title', dataIndex: 'title', key: 'title' },
			{ title: 'Name', dataIndex: 'name', key: 'name' },
			// { title: 'staffID', dataIndex: 'staff', key: 'staff' },
			{ title: 'Member Type', dataIndex: 'member', key: 'member' },
			{ title: 'Status', dataIndex: 'status', key: 'status' },
			{
				title: ' ',
				render: (text, record) => (
					<div className="action-column grid">
						<button
							className="edit column"
							onClick={() => {
								console.log(record);
								this.props.onEditClicked(record);
							}}
						>
							Edit
						</button>

						<button className="delete column" onClick={() => this.props.onDeleteClicked(record)}>
							Delete
						</button>
					</div>
				),
			},
		];

		return (
			<div className="teacher-list column">
				<div className="list-container">
					<div>
						<Row className="teacher-filter-option">
							<Col span={8}>
								<div className="filter-container-elements" />
							</Col>
							<Col span={8}>
								<div className="filter-container-elements" />
							</Col>
							<Col span={8}>
								<div className="search-container-elements">
									<Search placeholder="input search text" size="large" onChange={this.onSearch} />
								</div>
							</Col>
						</Row>
					</div>
					<h2>List of staff members</h2>
					<div className="table-container">
						<Table
							className="teacher-list-table"
							pagination={{ pageSize: this.state.height / 75 }}
							// scroll={{ y: 200 }}
							dataSource={this.state.dataSearch.length == 0 ? this.state.dataSearch : dataSource}
							columns={columns}
						/>
					</div>
				</div>
			</div>
		);
	}
}

TeacherList.propTypes = {
	onEditClicked: PropTypes.func.isRequired,
	onDeleteClicked: PropTypes.func.isRequired,

	dataSource: PropTypes.arrayOf(PropTypes.shape()),
};

TeacherList.defaultProps = {
	dataSource: [],
};
