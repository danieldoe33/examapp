import { ActionTypes } from '../../constants';

export function midsemLoaded() {
    // TODO: check inside SQLite for data
    const dataFromSQLite = [];

    return {
        type: ActionTypes.midsem.MIDSEMS_ADDED,
        payload: dataFromSQLite
    };
}

export function midsemEditted(payload: Object) {
    return {
        type: ActionTypes.midsem.MIDSEM_EDITTED,
        payload
    };
}

export function midsemAdded(payload: Object) {
    return {
        type: ActionTypes.midsem.MIDSEM_ADDED,
        payload
    };
}


export function midsemRemoved(payload: Object) {
    return {
        type: ActionTypes.midsem.MIDSEM_REMOVED,
        payload
    }
}